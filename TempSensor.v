/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module lcd_controller (init, clk, enable, bus, write_busy, lcd_rw, lcd_rs, lcd_enable, lcd_db);

input init;
input clk;
input enable;
input [10:0] bus;
output write_busy;
output lcd_rw;
output lcd_rs;
output lcd_enable;
output [7:0] lcd_db;

reg lcd_rs;
reg lcd_rw;
reg lcd_enable;
reg write_busy;
reg eval_spd;
reg [2:0] count;
reg [2:0] cur_state;
reg [2:0] next_state;
reg [7:0] lcd_db;
reg [10:0] bus;
reg [25:0] num_clks;

always @(posedge clk)
 begin
 cur_state=next_state;
 case(cur_state)

//-----LCD CONTROLLER MASTER--------//
3'b000:
begin
  if(init==1)
   begin
   num_clks=0;
   next_state=3'b111;
   end
  else 
   begin
    if(enable==1)
     begin
     eval_spd=bus[10];
     lcd_rs=bus[9];
     lcd_rw=bus[8];
     lcd_db=bus[7:0];
     num_clks=0;
     next_state=3'b010;
     end
    else
     begin
     eval_spd=0;
     lcd_rw=0;
     lcd_rs=0;
     lcd_db=0;
     next_state=3'b000;
     end
   end
end  
//-----END LCD CONTROLLER MASTER--------//
 

//-----INITIALIZATION FOR LCD DISPLAY--------// 
3'b111:
begin
 if(num_clks < 1000000)
  begin
  num_clks=num_clks+1;
  write_busy=1;
  next_state=3'b111;
  end
 else
  begin
  num_clks=0;
  next_state=3'b001;
  end
end


3'b001:
begin								
if(num_clks < 815601)
 begin
 write_busy=1; 
 if(num_clks < 275001)
  begin
  if (num_clks < 275000)
   begin
   lcd_db=8'b00111000;
   if (num_clks < 274000)
    begin
    lcd_enable=0;
    if (num_clks < 200)
     begin
     lcd_enable=1;
     if (num_clks < 100)       
      begin
      lcd_rs=0;
      lcd_rw=0;
      lcd_db=0;
      lcd_enable=0;
      end
     end
    end
   end
  else
   begin
   count=count+1;
   if (count < 3)
    begin
    num_clks=0;
    end
   end
  end
 else
  begin
  if (num_clks < 815600)
   begin
   lcd_db=8'b00000110;
   if (num_clks < 680600)
    begin
    lcd_enable=0;
    if (num_clks < 680500)
     begin
     lcd_enable=1;
     if (num_clks < 680400)
      begin
      lcd_enable=0;
      lcd_db=8'b00000001;
      if (num_clks < 545400)
       begin
       lcd_enable=0;
       if (num_clks < 545300)
        begin
        lcd_enable=1;
        if (num_clks < 545200)
         begin
         lcd_enable=0;
         lcd_db=8'b00001111;
         if (num_clks < 410200)
          begin
          lcd_enable=0;
          if (num_clks < 410100)
           begin
           lcd_enable=1;
           if (num_clks < 410001)
            begin
            lcd_enable=0;
            lcd_db=8'b00111000;
            if (num_clks < 275200)
             begin
             lcd_enable=0;
             if (num_clks < 275100)
              begin
              lcd_enable=1;
              end
             end
            end
           end
          end
         end
        end
       end
      end
     end
    end
   end
  end
 num_clks=num_clks+1;
 next_state=3'b001;
 end
else
 begin
 num_clks=0;
 write_busy=0;
 count=0;
 next_state=3'b000;
 end
end
//-------END OF INITIALIZATION OF LCD DISPLAY-------------//


//---------EVALUATION OF INPUT BUS--------------//
3'b010:
begin
  if(eval_spd==1)
   begin
   next_state=3'b011;
   end
  else
   begin
   next_state=3'b100;
   end
end
//---------END EVALUATION OF INPUT BUS--------------//


//------WRITING TO LCD - FAST-------//
3'b011:
begin
     if (num_clks < 1003000)
       begin
        write_busy=1;
        if (num_clks < 500)
         begin
           lcd_enable=0;
           if (num_clks < 100)
            begin
             lcd_enable=1;
            end 
         end
        num_clks=num_clks+1;
       end
     else
       begin
         num_clks=0;
         write_busy=0;
       end
end
//-----END WRITING TO LCD - FAST-----//


//------WRITING TO LCD - SLOW-------//
3'b100:
begin
      if (num_clks < 1003000)
       begin
        write_busy=1;
        if (num_clks < 500)
         begin
           lcd_enable=0;
           if (num_clks < 100)
            begin
             lcd_enable=1;
            end 
         end
        num_clks=num_clks+1;
       end
     else
       begin
         num_clks=0;
         write_busy=0;
         next_state=3'b000;
       end
end
//-----END WRITING TO LCD - SLOW-----//

3'b101:
begin
next_state=3'b000;
end

3'b110:
begin
next_state=3'b000;
end

/*3'b111:
begin
next_state=3'b000;
end*/

    endcase
  end
endmodule