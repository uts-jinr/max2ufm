
module UFM_WR (
	addr,
	nread,
	dataout,
	nbusy,
	data_valid,
	datain,
	nwrite,
	nerase);	

	input	[8:0]	addr;
	input		nread;
	output	[15:0]	dataout;
	output		nbusy;
	output		data_valid;
	input	[15:0]	datain;
	input		nwrite;
	input		nerase;
endmodule
