//altufm_parallel ACCESS_MODE="READ_WRITE" CBX_AUTO_BLACKBOX="ALL" CBX_SINGLE_OUTPUT_FILE="ON" DEVICE_FAMILY="MAX II" ERASE_TIME=500000000 LPM_FILE="D:/FPGA/MAX2UFM/MAX2UFM_03/InverseAddress.mif" OSC_FREQUENCY=180000 PROGRAM_TIME=1600000 WIDTH_ADDRESS=9 WIDTH_DATA=16 WIDTH_UFM_ADDRESS=9 addr data_valid datain dataout nbusy nerase nread nwrite
//VERSION_BEGIN 15.1 cbx_a_gray2bin 2015:10:21:18:09:22:SJ cbx_a_graycounter 2015:10:21:18:09:22:SJ cbx_altufm_parallel 2015:10:21:18:09:23:SJ cbx_cycloneii 2015:10:21:18:09:23:SJ cbx_lpm_add_sub 2015:10:21:18:09:23:SJ cbx_lpm_compare 2015:10:21:18:09:23:SJ cbx_lpm_counter 2015:10:21:18:09:23:SJ cbx_lpm_decode 2015:10:21:18:09:23:SJ cbx_lpm_mux 2015:10:21:18:09:23:SJ cbx_maxii 2015:10:21:18:09:23:SJ cbx_mgl 2015:10:21:18:12:49:SJ cbx_nadder 2015:10:21:18:09:23:SJ cbx_stratix 2015:10:21:18:09:23:SJ cbx_stratixii 2015:10:21:18:09:23:SJ cbx_util_mgl 2015:10:21:18:09:23:SJ  VERSION_END
// synthesis VERILOG_INPUT_VERSION VERILOG_2001
// altera message_off 10463



// Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
//  Your use of Altera Corporation's design tools, logic functions 
//  and other software and tools, and its AMPP partner logic 
//  functions, and any output files from any of the foregoing 
//  (including device programming or simulation files), and any 
//  associated documentation or information are expressly subject 
//  to the terms and conditions of the Altera Program License 
//  Subscription Agreement, the Altera Quartus Prime License Agreement,
//  the Altera MegaCore Function License Agreement, or other 
//  applicable license agreement, including, without limitation, 
//  that your use is for the sole purpose of programming logic 
//  devices manufactured by Altera and sold by Altera or its 
//  authorized distributors.  Please refer to the applicable 
//  agreement for further details.



//synthesis_resources = lpm_counter 1 lut 79 maxii_ufm 1 
//synopsys translate_off
`timescale 1 ps / 1 ps
//synopsys translate_on
(* ALTERA_ATTRIBUTE = {"suppress_da_rule_internal=c101;suppress_da_rule_internal=c103;suppress_da_rule_internal=c104;suppress_da_rule_internal=r101;suppress_da_rule_internal=s104;suppress_da_rule_internal=s102"} *)
module  UFM_WR_ufm_parallel_0
	( 
	addr,
	data_valid,
	datain,
	dataout,
	nbusy,
	nerase,
	nread,
	nwrite) /* synthesis synthesis_clearbox=1 */;
	input   [8:0]  addr;
	output   data_valid;
	input   [15:0]  datain;
	output   [15:0]  dataout;
	output   nbusy;
	input   nerase;
	input   nread;
	input   nwrite;
`ifndef ALTERA_RESERVED_QIS
// synopsys translate_off
`endif
	tri0   [15:0]  datain;
	tri1   nerase;
	tri1   nwrite;
`ifndef ALTERA_RESERVED_QIS
// synopsys translate_on
`endif

	reg	[8:0]	A;
	reg	data_valid_out_reg;
	reg	data_valid_reg;
	reg	deco1_dffe;
	reg	deco2_dffe;
	reg	deco3_dffe;
	reg	decode_dffe;
	reg	gated_clk1_dffe;
	reg	gated_clk2_dffe;
	reg	[15:0]	piso_sipo_dffe;
	reg	program_dffe;
	wire	wire_program_dffe_clrn;
	reg	real_decode2_dffe;
	reg	real_decode_dffe;
	wire	[15:0]	wire_tmp_do_d;
	reg	[15:0]	tmp_do;
	wire	[15:0]	wire_tmp_do_ena;
	wire  [4:0]   wire_cntr2_q;
	wire  wire_maxii_ufm_block1_bgpbusy;
	wire  wire_maxii_ufm_block1_busy;
	wire  wire_maxii_ufm_block1_drdout;
	wire  wire_maxii_ufm_block1_osc;
	wire  add_en;
	wire  add_load;
	wire  arclk;
	wire  busy_arclk;
	wire  busy_drclk;
	wire  control_mux;
	wire  copy_tmp_decode;
	wire  data_en;
	wire  data_load;
	wire  data_valid_en;
	wire  dly_tmp_decode;
	wire  drclk;
	wire  drshft;
	wire  erase;
	wire  erase_op;
	wire  gated1;
	wire  gated2;
	wire  hold_decode;
	wire  in_erase;
	wire  in_program;
	wire  in_read_data_en;
	wire  in_read_drclk;
	wire  in_read_drshft;
	wire  in_write_data_en;
	wire  in_write_data_load;
	wire  in_write_drclk;
	wire  in_write_drshft;
	wire  mux_nerase;
	wire  mux_nread;
	wire  mux_nwrite;
	wire oscena;
	wire  [15:0]  piso_sipo_d;
	wire  [15:0]  piso_sipo_q;
	wire  program1;
	wire  q0;
	wire  q1;
	wire  q2;
	wire  q3;
	wire  q4;
	wire  read_data_en;
	wire  read_drclk;
	wire  read_drshft;
	wire  read_op;
	wire  real_decode;
	wire  [8:0]  shiftin;
	wire  start_decode;
	wire  start_op;
	wire  stop_op;
	wire  tmp_add_en;
	wire  tmp_add_load;
	wire  tmp_arclk;
	wire  tmp_arclk0;
	wire  tmp_ardin;
	wire  tmp_arshft;
	wire  tmp_data_valid2;
	wire  tmp_decode;
	wire  [15:0]  tmp_di;
	wire  tmp_drclk;
	wire  tmp_drdin;
	wire  tmp_erase;
	wire  tmp_read;
	wire  tmp_write;
	wire  ufm_arclk;
	wire  ufm_ardin;
	wire  ufm_arshft;
	wire  ufm_bgpbusy;
	wire  ufm_busy;
	wire  ufm_drclk;
	wire  ufm_drdin;
	wire  ufm_drdout;
	wire  ufm_drshft;
	wire  ufm_erase;
	wire  ufm_osc;
	wire  ufm_oscena;
	wire  ufm_program;
	wire  write_data_en;
	wire  write_data_load;
	wire  write_drclk;
	wire  write_drshft;
	wire  write_op;
	wire  [8:0]  X_var;
	wire  [8:0]  Y_var;
	wire  [8:0]  Z_var;

	// synopsys translate_off
	initial
		A = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (add_en == 1'b1)   A <= {Z_var};
	// synopsys translate_off
	initial
		data_valid_out_reg = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  data_valid_out_reg <= (data_valid_reg & (~ tmp_decode));
	// synopsys translate_off
	initial
		data_valid_reg = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (data_valid_en == 1'b1)   data_valid_reg <= tmp_data_valid2;
	// synopsys translate_off
	initial
		deco1_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (start_op == 1'b1)   deco1_dffe <= mux_nread;
	// synopsys translate_off
	initial
		deco2_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (start_op == 1'b1)   deco2_dffe <= mux_nwrite;
	// synopsys translate_off
	initial
		deco3_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (start_op == 1'b1)   deco3_dffe <= mux_nerase;
	// synopsys translate_off
	initial
		decode_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  decode_dffe <= copy_tmp_decode;
	// synopsys translate_off
	initial
		gated_clk1_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  gated_clk1_dffe <= busy_arclk;
	// synopsys translate_off
	initial
		gated_clk2_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  gated_clk2_dffe <= busy_drclk;
	// synopsys translate_off
	initial
		piso_sipo_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (data_en == 1'b1)   piso_sipo_dffe <= {piso_sipo_d[15:0]};
	// synopsys translate_off
	initial
		program_dffe = 0;
	// synopsys translate_on
	always @ ( negedge ufm_busy or  negedge wire_program_dffe_clrn)
		if (wire_program_dffe_clrn == 1'b0) program_dffe <= 1'b0;
		else  program_dffe <= (ufm_program | ufm_erase);
	assign
		wire_program_dffe_clrn = (~ (stop_op | (((((~ q4) & (~ q3)) & (~ q2)) & (~ q1)) & (~ q0))));
	// synopsys translate_off
	initial
		real_decode2_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  real_decode2_dffe <= real_decode_dffe;
	// synopsys translate_off
	initial
		real_decode_dffe = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		  real_decode_dffe <= start_decode;
	// synopsys translate_off
	initial
		tmp_do[0:0] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[0:0] == 1'b1)   tmp_do[0:0] <= wire_tmp_do_d[0:0];
	// synopsys translate_off
	initial
		tmp_do[1:1] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[1:1] == 1'b1)   tmp_do[1:1] <= wire_tmp_do_d[1:1];
	// synopsys translate_off
	initial
		tmp_do[2:2] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[2:2] == 1'b1)   tmp_do[2:2] <= wire_tmp_do_d[2:2];
	// synopsys translate_off
	initial
		tmp_do[3:3] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[3:3] == 1'b1)   tmp_do[3:3] <= wire_tmp_do_d[3:3];
	// synopsys translate_off
	initial
		tmp_do[4:4] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[4:4] == 1'b1)   tmp_do[4:4] <= wire_tmp_do_d[4:4];
	// synopsys translate_off
	initial
		tmp_do[5:5] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[5:5] == 1'b1)   tmp_do[5:5] <= wire_tmp_do_d[5:5];
	// synopsys translate_off
	initial
		tmp_do[6:6] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[6:6] == 1'b1)   tmp_do[6:6] <= wire_tmp_do_d[6:6];
	// synopsys translate_off
	initial
		tmp_do[7:7] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[7:7] == 1'b1)   tmp_do[7:7] <= wire_tmp_do_d[7:7];
	// synopsys translate_off
	initial
		tmp_do[8:8] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[8:8] == 1'b1)   tmp_do[8:8] <= wire_tmp_do_d[8:8];
	// synopsys translate_off
	initial
		tmp_do[9:9] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[9:9] == 1'b1)   tmp_do[9:9] <= wire_tmp_do_d[9:9];
	// synopsys translate_off
	initial
		tmp_do[10:10] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[10:10] == 1'b1)   tmp_do[10:10] <= wire_tmp_do_d[10:10];
	// synopsys translate_off
	initial
		tmp_do[11:11] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[11:11] == 1'b1)   tmp_do[11:11] <= wire_tmp_do_d[11:11];
	// synopsys translate_off
	initial
		tmp_do[12:12] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[12:12] == 1'b1)   tmp_do[12:12] <= wire_tmp_do_d[12:12];
	// synopsys translate_off
	initial
		tmp_do[13:13] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[13:13] == 1'b1)   tmp_do[13:13] <= wire_tmp_do_d[13:13];
	// synopsys translate_off
	initial
		tmp_do[14:14] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[14:14] == 1'b1)   tmp_do[14:14] <= wire_tmp_do_d[14:14];
	// synopsys translate_off
	initial
		tmp_do[15:15] = 0;
	// synopsys translate_on
	always @ ( posedge ufm_osc)
		if (wire_tmp_do_ena[15:15] == 1'b1)   tmp_do[15:15] <= wire_tmp_do_d[15:15];
	assign
		wire_tmp_do_d = {piso_sipo_q[15:0]};
	assign
		wire_tmp_do_ena = {16{(data_valid_reg & (~ tmp_decode))}};
	lpm_counter   cntr2
	( 
	.clk_en(tmp_decode),
	.clock(ufm_osc),
	.cnt_en(((~ (ufm_program | ufm_erase)) | program1)),
	.cout(),
	.eq(),
	.q(wire_cntr2_q)
	`ifndef FORMAL_VERIFICATION
	// synopsys translate_off
	`endif
	,
	.aclr(1'b0),
	.aload(1'b0),
	.aset(1'b0),
	.cin(1'b1),
	.data({5{1'b0}}),
	.sclr(1'b0),
	.sload(1'b0),
	.sset(1'b0),
	.updown(1'b1)
	`ifndef FORMAL_VERIFICATION
	// synopsys translate_on
	`endif
	);
	defparam
		cntr2.lpm_direction = "UP",
		cntr2.lpm_modulus = 28,
		cntr2.lpm_port_updown = "PORT_UNUSED",
		cntr2.lpm_width = 5,
		cntr2.lpm_type = "lpm_counter";
	maxii_ufm   maxii_ufm_block1
	( 
	.arclk(ufm_arclk),
	.ardin(ufm_ardin),
	.arshft(ufm_arshft),
	.bgpbusy(wire_maxii_ufm_block1_bgpbusy),
	.busy(wire_maxii_ufm_block1_busy),
	.drclk(ufm_drclk),
	.drdin(ufm_drdin),
	.drdout(wire_maxii_ufm_block1_drdout),
	.drshft(ufm_drshft),
	.erase(ufm_erase),
	.osc(wire_maxii_ufm_block1_osc),
	.oscena(ufm_oscena),
	.program(ufm_program)
	// synopsys translate_off
	,
	.ctrl_bgpbusy(1'b0),
	.devclrn(1'b1),
	.devpor(1'b1),
	.sbdin(1'b0),
	.sbdout()
	// synopsys translate_on
	);
	defparam
		maxii_ufm_block1.address_width = 9,
		maxii_ufm_block1.erase_time = 500000000,
		maxii_ufm_block1.init_file = "D:/FPGA/MAX2UFM/MAX2UFM_03/InverseAddress.mif",
		maxii_ufm_block1.mem1 = 512'hFFE0FFE1FFE2FFE3FFE4FFE5FFE6FFE7FFE8FFE9FFEAFFEBFFECFFEDFFEEFFEFFFF0FFF1FFF2FFF3FFF4FFF5FFF6FFF7FFF8FFF9FFFAFFFBFFFCFFFDFFFEABCD,
		maxii_ufm_block1.mem10 = 512'hFEC0FEC1FEC2FEC3FEC4FEC5FEC6FEC7FEC8FEC9FECAFECBFECCFECDFECEFECFFED0FED1FED2FED3FED4FED5FED6FED7FED8FED9FEDAFEDBFEDCFEDDFEDEFEDF,
		maxii_ufm_block1.mem11 = 512'hFEA0FEA1FEA2FEA3FEA4FEA5FEA6FEA7FEA8FEA9FEAAFEABFEACFEADFEAEFEAFFEB0FEB1FEB2FEB3FEB4FEB5FEB6FEB7FEB8FEB9FEBAFEBBFEBCFEBDFEBEFEBF,
		maxii_ufm_block1.mem12 = 512'hFE80FE81FE82FE83FE84FE85FE86FE87FE88FE89FE8AFE8BFE8CFE8DFE8EFE8FFE90FE91FE92FE93FE94FE95FE96FE97FE98FE99FE9AFE9BFE9CFE9DFE9EFE9F,
		maxii_ufm_block1.mem13 = 512'hFE60FE61FE62FE63FE64FE65FE66FE67FE68FE69FE6AFE6BFE6CFE6DFE6EFE6FFE70FE71FE72FE73FE74FE75FE76FE77FE78FE79FE7AFE7BFE7CFE7DFE7EFE7F,
		maxii_ufm_block1.mem14 = 512'hFE40FE41FE42FE43FE44FE45FE46FE47FE48FE49FE4AFE4BFE4CFE4DFE4EFE4FFE50FE51FE52FE53FE54FE55FE56FE57FE58FE59FE5AFE5BFE5CFE5DFE5EFE5F,
		maxii_ufm_block1.mem15 = 512'hFE20FE21FE22FE23FE24FE25FE26FE27FE28FE29FE2AFE2BFE2CFE2DFE2EFE2FFE30FE31FE32FE33FE34FE35FE36FE37FE38FE39FE3AFE3BFE3CFE3DFE3EFE3F,
		maxii_ufm_block1.mem16 = 512'h8F80FE01FE02FE03FE04FE05FE06FE07FE08FE09FE0AFE0BFE0CFE0DFE0EFE0FFE10FE11FE12FE13FE14FE15FE16FE17FE18FE19FE1AFE1BFE1CFE1DFE1EFE1F,
		maxii_ufm_block1.mem2 = 512'hFFC0FFC1FFC2FFC3FFC4FFC5FFC6FFC7FFC8FFC9FFCAFFCBFFCCFFCDFFCEFFCFFFD0FFD1FFD2FFD3FFD4FFD5FFD6FFD7FFD8FFD9FFDAFFDBFFDCFFDDFFDEFFDF,
		maxii_ufm_block1.mem3 = 512'hFFA0FFA1FFA2FFA3FFA4FFA5FFA6FFA7FFA8FFA9FFAAFFABFFACFFADFFAEFFAFFFB0FFB1FFB2FFB3FFB4FFB5FFB6FFB7FFB8FFB9FFBAFFBBFFBCFFBDFFBEFFBF,
		maxii_ufm_block1.mem4 = 512'hFF80FF81FF82FF83FF84FF85FF86FF87FF88FF89FF8AFF8BFF8CFF8DFF8EFF8FFF90FF91FF92FF93FF94FF95FF96FF97FF98FF99FF9AFF9BFF9CFF9DFF9EFF9F,
		maxii_ufm_block1.mem5 = 512'hFF60FF61FF62FF63FF64FF65FF66FF67FF68FF69FF6AFF6BFF6CFF6DFF6EFF6FFF70FF71FF72FF73FF74FF75FF76FF77FF78FF79FF7AFF7BFF7CFF7DFF7EFF7F,
		maxii_ufm_block1.mem6 = 512'hFF40FF41FF42FF43FF44FF45FF46FF47FF48FF49FF4AFF4BFF4CFF4DFF4EFF4FFF50FF51FF52FF53FF54FF55FF56FF57FF58FF59FF5AFF5BFF5CFF5DFF5EFF5F,
		maxii_ufm_block1.mem7 = 512'hFF20FF21FF22FF23FF24FF25FF26FF27FF28FF29FF2AFF2BFF2CFF2DFF2EFF2FFF30FF31FF32FF33FF34FF35FF36FF37FF38FF39FF3AFF3BFF3CFF3DFF3EFF3F,
		maxii_ufm_block1.mem8 = 512'hFF00FF01FF02FF03FF04FF05FF06FF07FF08FF09FF0AFF0BFF0CFF0DFF0EFF0FFF10FF11FF12FF13FF14FF15FF16FF17FF18FF19FF1AFF1BFF1CFF1DFF1EFF1F,
		maxii_ufm_block1.mem9 = 512'hFEE0FEE1FEE2FEE3FEE4FEE5FEE6FEE7FEE8FEE9FEEAFEEBFEECFEEDFEEEFEEFFEF0FEF1FEF2FEF3FEF4FEF5FEF6FEF7FEF8FEF9FEFAFEFBFEFCFEFDFEFEFEFF,
		maxii_ufm_block1.osc_sim_setting = 180000,
		maxii_ufm_block1.program_time = 1600000,
		maxii_ufm_block1.lpm_type = "maxii_ufm";
	assign
		add_en = (tmp_add_en & ((read_op | write_op) | erase_op)),
		add_load = (tmp_add_load & ((read_op | write_op) | erase_op)),
		arclk = (tmp_arclk0 & ((read_op | write_op) | erase_op)),
		busy_arclk = arclk,
		busy_drclk = drclk,
		control_mux = (((~ q4) & ((q3 | q2) | q1)) | q4),
		copy_tmp_decode = tmp_decode,
		data_en = (((~ read_op) & write_data_en) | (read_op & read_data_en)),
		data_load = (write_data_load & write_op),
		data_valid = data_valid_out_reg,
		data_valid_en = ((q4 & q3) & q1),
		dataout = tmp_do,
		dly_tmp_decode = decode_dffe,
		drclk = (((~ read_op) & write_drclk) | (read_op & read_drclk)),
		drshft = (((~ read_op) & write_drshft) | (read_op & read_drshft)),
		erase = (in_erase & erase_op),
		erase_op = ((tmp_read & tmp_write) & (~ tmp_erase)),
		gated1 = gated_clk1_dffe,
		gated2 = gated_clk2_dffe,
		hold_decode = ((~ real_decode2_dffe) & real_decode),
		in_erase = (((((~ q4) & q3) & (~ q2)) & q1) & q0),
		in_program = ((((q4 & (~ q3)) & (~ q2)) & (~ q1)) & q0),
		in_read_data_en = (((~ q4) & ((q3 & q2) | (q3 & q1))) | (q4 & (((~ q3) | ((~ q2) & (~ q1))) | (q1 & (~ q0))))),
		in_read_drclk = (((~ q4) & ((q3 & q2) | (q3 & q1))) | (q4 & (((~ q3) | ((~ q2) & (~ q1))) | (q1 & (~ q0))))),
		in_read_drshft = (~ (((((~ q4) & q3) & (~ q2)) & q1) & q0)),
		in_write_data_en = ((~ q4) | ((((~ q3) & (~ q2)) & (~ q1)) & (~ q0))),
		in_write_data_load = (~ (((~ q4) & (((q3 | q2) | q1) | q0)) | (q4 & ((((~ q3) & (~ q2)) & (~ q1)) & (~ q0))))),
		in_write_drclk = (~ q4),
		in_write_drshft = in_write_data_en,
		mux_nerase = (((~ control_mux) & nerase) | (control_mux & data_valid_en)),
		mux_nread = (((~ control_mux) & nread) | (control_mux & data_valid_en)),
		mux_nwrite = (((~ control_mux) & nwrite) | (control_mux & data_valid_en)),
		nbusy = ((~ dly_tmp_decode) & (~ ufm_bgpbusy)),
		oscena = 1'b1,
		piso_sipo_d = {((piso_sipo_q[14] & (~ data_load)) | (tmp_di[15] & data_load)), ((piso_sipo_q[13] & (~ data_load)) | (tmp_di[14] & data_load)), ((piso_sipo_q[12] & (~ data_load)) | (tmp_di[13] & data_load)), ((piso_sipo_q[11] & (~ data_load)) | (tmp_di[12] & data_load)), ((piso_sipo_q[10] & (~ data_load)) | (tmp_di[11] & data_load)), ((piso_sipo_q[9] & (~ data_load)) | (tmp_di[10] & data_load)), ((piso_sipo_q[8] & (~ data_load)) | (tmp_di[9] & data_load)), ((piso_sipo_q[7] & (~ data_load)) | (tmp_di[8] & data_load)), ((piso_sipo_q[6] & (~ data_load)) | (tmp_di[7] & data_load)), ((piso_sipo_q[5] & (~ data_load)) | (tmp_di[6] & data_load)), ((piso_sipo_q[4] & (~ data_load)) | (tmp_di[5] & data_load)), ((piso_sipo_q[3] & (~ data_load)) | (tmp_di[4] & data_load)), ((piso_sipo_q[2] & (~ data_load)) | (tmp_di[3] & data_load)), ((piso_sipo_q[1] & (~ data_load)) | (tmp_di[2] & data_load)), ((piso_sipo_q[0] & (~ data_load)) | (tmp_di[1] & data_load)), ((ufm_drdout & (~ data_load)) | (tmp_di[0] & data_load))},
		piso_sipo_q = {piso_sipo_dffe[15:0]},
		program1 = program_dffe,
		q0 = wire_cntr2_q[0],
		q1 = wire_cntr2_q[1],
		q2 = wire_cntr2_q[2],
		q3 = wire_cntr2_q[3],
		q4 = wire_cntr2_q[4],
		read_data_en = (in_read_data_en & read_op),
		read_drclk = (in_read_drclk & read_op),
		read_drshft = (in_read_drshft & read_op),
		read_op = (((~ tmp_read) & tmp_write) & tmp_erase),
		real_decode = start_decode,
		shiftin = {A[7:0], 1'b0},
		start_decode = ((~ ufm_bgpbusy) & (((((~ mux_nread) & mux_nwrite) & mux_nerase) | ((mux_nread & (~ mux_nwrite)) & mux_nerase)) | ((mux_nread & mux_nwrite) & (~ mux_nerase)))),
		start_op = (hold_decode | stop_op),
		stop_op = ((((q4 & q3) & (~ q2)) & q1) & q0),
		tmp_add_en = ((~ q4) & ((~ q3) | ((~ q2) & (~ q1)))),
		tmp_add_load = (~ ((~ q4) & (((((~ q3) & q2) | ((~ q3) & q0)) | ((~ q3) & q1)) | ((q3 & (~ q2)) & (~ q1))))),
		tmp_arclk = (gated1 & (~ ufm_osc)),
		tmp_arclk0 = ((~ q4) & ((~ q3) | (((~ q2) & (~ q1)) & (~ q0)))),
		tmp_ardin = A[8],
		tmp_arshft = add_en,
		tmp_data_valid2 = (stop_op & read_op),
		tmp_decode = (((((~ tmp_read) & tmp_write) & tmp_erase) | ((tmp_read & (~ tmp_write)) & tmp_erase)) | ((tmp_read & tmp_write) & (~ tmp_erase))),
		tmp_di = {datain[15:0]},
		tmp_drclk = (gated2 & (~ ufm_osc)),
		tmp_drdin = piso_sipo_dffe[15],
		tmp_erase = deco3_dffe,
		tmp_read = deco1_dffe,
		tmp_write = deco2_dffe,
		ufm_arclk = tmp_arclk,
		ufm_ardin = tmp_ardin,
		ufm_arshft = tmp_arshft,
		ufm_bgpbusy = wire_maxii_ufm_block1_bgpbusy,
		ufm_busy = wire_maxii_ufm_block1_busy,
		ufm_drclk = tmp_drclk,
		ufm_drdin = tmp_drdin,
		ufm_drdout = wire_maxii_ufm_block1_drdout,
		ufm_drshft = drshft,
		ufm_erase = ((erase | ufm_busy) & erase_op),
		ufm_osc = wire_maxii_ufm_block1_osc,
		ufm_oscena = oscena,
		ufm_program = (in_program & write_op),
		write_data_en = (in_write_data_en & write_op),
		write_data_load = (in_write_data_load & write_op),
		write_drclk = (in_write_drclk & write_op),
		write_drshft = (in_write_drshft & write_op),
		write_op = ((tmp_read & (~ tmp_write)) & tmp_erase),
		X_var = (shiftin & {9{(~ add_load)}}),
		Y_var = (addr & {9{add_load}}),
		Z_var = (X_var | Y_var);
endmodule //UFM_WR_ufm_parallel_0
//VALID FILE
