module SRAMInterface (
//Inputs
Clock, 
Reset, 
EnableSRAMWrite,
EnableSRAMRead,
USBCanWrite_n,
USBDataPresent_n,
DataInFromUSB,
DataFromSRAM,
//Outputs 
USBReadFifo,
USBWriteFifo, 
Data2USB,
Addr2SRAM,
TriEnableDin_n,
TriEnableDout_n,
SRAMce_n,
SRAMoe_n,
SRAMwe_n,
SRAMTriEnableDin,
SRAMTriEnableDout,
Data2SRAM,
WritingSRAM,
ReadingSRAM,
Error
);

input Clock; 
input Reset;
input EnableSRAMWrite;
input EnableSRAMRead;
input USBCanWrite_n;
input USBDataPresent_n;
input [7:0] DataInFromUSB;
input [7:0] DataFromSRAM;
output USBReadFifo;
output USBWriteFifo;
output TriEnableDin_n;
output TriEnableDout_n;
output [7:0] Data2USB; 
output [16:0] Addr2SRAM;
output SRAMce_n;
output SRAMoe_n;
output SRAMwe_n;
output SRAMTriEnableDin;
output SRAMTriEnableDout;
output [7:0] Data2SRAM;
output WritingSRAM;
output ReadingSRAM;
output Error;
wire [7:0] DataInFromUSB;
reg USBWriteFifo;
reg [16:0] Addr2SRAM; 
reg WritingSRAM; 
reg ReadingSRAM;
//Internal Sigs
reg [2:0] num_clks;
reg [4:0] NextState;
reg [4:0] CurState; 
reg USBReadFifo;
reg TriEnableDin_n;
reg TriEnableDout_n;
reg [7:0] Data2USB;
reg [3:0] LEDs;
reg SRAMce_n;
reg SRAMoe_n;
reg SRAMwe_n;
reg SRAMTriEnableDin;
reg SRAMTriEnableDout;
reg [7:0] Data2SRAM;
reg USBDataPresent_nRega;
reg USBDataPresent_nRegb;
reg USBCanWrite_nRega;
reg USBCanWrite_nRegb;
reg Error;

//Register asynchronous inputs
always @(posedge Clock)
begin
USBDataPresent_nRega <= USBDataPresent_n;
USBDataPresent_nRegb <= USBDataPresent_nRega;
end

always @(posedge Clock)
begin
USBCanWrite_nRega <= USBCanWrite_n;
USBCanWrite_nRegb <= USBCanWrite_nRega;
end


always @(posedge Clock)
begin
	if (Reset == 0) //if reset is 0 then we stay in S0.
		begin
		NextState = 5'b00000;
		USBReadFifo = 1'b0;
		USBWriteFifo = 1'b0;
		TriEnableDin_n = 1'b0;
		TriEnableDout_n = 1'b0;
		Data2USB [7:0] = 8'b00000000; 
		Addr2SRAM [16:0] = 17'h00000;
		SRAMce_n = 1'b1;
		SRAMoe_n = 1'b1;
		SRAMwe_n = 1'b1;
		SRAMTriEnableDin = 1'b0;
		SRAMTriEnableDout = 1'b0;
		Data2SRAM [7:0] = 8'b00000000;
		WritingSRAM = 1'b0;
		ReadingSRAM = 1'b0;
		Error = 1'b0;
		end
	else //now things get exciting. . .  
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			 
			5'b00000:  //State 0 Wait until the controller is enabled to either write or read the sram*/
			if (EnableSRAMWrite == 1'b1) 
				begin
				NextState = 5'b00001;
				WritingSRAM = 1'b1;
				end				
			else if (EnableSRAMRead == 1'b1) 
				begin 
				NextState = 5'b01011;
				ReadingSRAM = 1'b1;
				end
			else
				begin
				NextState = 5'b00000;
				USBReadFifo = 1'b0;
				USBWriteFifo = 1'b0;
				TriEnableDin_n = 1'b0;
				TriEnableDout_n = 1'b0;
				Data2USB [7:0] = 8'b00000000; 
				Addr2SRAM [16:0] = 17'b00000000000000000;
				SRAMce_n = 1'b1;
				SRAMoe_n = 1'b1;
				SRAMwe_n = 1'b1;
				SRAMTriEnableDin = 1'b0;
				SRAMTriEnableDout = 1'b0;
				Data2SRAM [7:0] = 8'b00000000;
				WritingSRAM = 1'b0;
				ReadingSRAM =1'b0;
				Error = 1'b0;
				end
			5'b00001:  //State 1 Wait to receive the first byte to write to the SRAM
			if  ((USBDataPresent_nRegb == 0) && (num_clks == 3'b111))
				begin
				NextState = 5'b00010;  //s2
				num_clks = 3'b000;			
				end
			else 
				begin 
				NextState = 5'b00001;  //s1
				num_clks = num_clks + 1'b1;
				end
	
			5'b00010:  //State 2: After USBReadFifo goes low it takes a max of 50ns for data to be
			//present on USBData[7:0], since the clock is at 66.6 MHz, the period 
			//= ~15ns, so wait for 5 clocks before latching it
			if (num_clks == 3'b011)
				begin
				NextState = 5'b00011; //s3 
				USBReadFifo = 1'b1; 
				num_clks = 3'b000; 
				end	
			else
				begin 
				NextState = 5'b00010; //s2
				num_clks = num_clks + 1'b1;
				TriEnableDin_n = 1'b1; //enable data bus (from FTDI to M2)
				end
			5'b00011:  //State 3 Latch the data
			begin 
			NextState = 5'b00100;
			Data2SRAM = DataInFromUSB;
			end
			5'b00100:  //State 4 De-assert the USBReadFifo and we disable our USB tri state
			begin
			NextState = 5'b00101;
			USBReadFifo = 1'b0;
			TriEnableDin_n = 1'b0; //disable dbus (FTDI to M2)
			end
			5'b00101: //State 5 Assert the SRAM chip enable
			begin
			NextState = 5'b00110;  //s6
			SRAMce_n = 1'b0;
			SRAMTriEnableDout = 1'b1;  
			end
			5'b00110:  //State 6 Write to the SRAM by asserting SRAM write enable
			begin
			NextState = 5'b00111;  //s7
			SRAMwe_n = 1'b0;
			end
			5'b00111:  //State 7 De-assert SRAM controls and tri-state
			begin
			NextState = 5'b01000;  //s8
			SRAMce_n = 1'b1;
			SRAMTriEnableDout = 1'b0;
			SRAMwe_n = 1'b1;
			end
			5'b01000:  //State 8
			if (Addr2SRAM == 17'h0017F)
				begin
				NextState = 5'b00000;
				WritingSRAM = 1'b0;
				Addr2SRAM = 17'b00000;
				end
			else
				begin //we are still writing to SRAM
				NextState = 5'b00001;
				Addr2SRAM = Addr2SRAM + 1'b1;
				end
/*Begin READ SRAM Circuit */
			5'b01001:  //State 9 ensure write enable is deasserted.
				begin
				NextState = 5'b01010;  //s10
				SRAMwe_n = 1'b1;
				end
			5'b01010: //State 10 Reading from SRAM
				begin   					
				NextState = 5'b01011;  //s11
				end
			5'b01011:  //State 11 Assert SRAM output enable
			if (num_clks == 3'b011) 
				begin
				NextState = 5'b01100;  //s12
				num_clks = 3'b000;
				SRAMoe_n = 1'b0;
				end
			else
				begin 
				NextState = 5'b01011;  //s11
				SRAMTriEnableDin = 1'b1; 
				num_clks = num_clks + 1'b1;
				SRAMce_n = 1'b0;
				end
			5'b01100:  //State 12 Now we'll latch the data from the sram
				begin
				NextState = 5'b01101; // 13 go write it to the pc
				Data2USB [7:0] = DataFromSRAM [7:0]; 
				end
			5'b01101:  //State 13 We now have the data to send to the pc on USBData
			if ((USBDataPresent_nRegb == 1) && (USBCanWrite_nRegb == 1'b0))
				begin
				NextState = 5'b01110;  //s14
				TriEnableDout_n = 1'b1;
				end	
			else
				begin
				NextState = 5'b01101;  //s13
				end
			5'b01110: //State 14 we need to de-assert USBWriteFifo after > 50ns so we'll wait 
					//five clocks to do so
				if (num_clks == 3'b011)
					begin
					NextState = 5'b01111;  //s15
					USBWriteFifo = 1'b0;   //strobing USBWriteFifo from high to low writes the data to the USB Mac
					num_clks = 3'b000;
					TriEnableDout_n = 1'b0;
					Addr2SRAM [16:0] = Addr2SRAM[16:0] + 1'b1; // increment the sram address
					end
				else 
					begin
					NextState = 5'b01110;  //s14
					num_clks = num_clks + 1'b1; 
					USBWriteFifo = 1'b1;
					SRAMTriEnableDin = 1'b0;
					SRAMoe_n = 1'b1;
					SRAMce_n = 1'b1;
					end
			5'b01111:  //State 15  Check to see if we've finished this command
			if (Addr2SRAM == 17'h00180)
				begin
				NextState = 5'b00000;  //s0 finished
				Addr2SRAM [16:0] = 17'h00000;
				ReadingSRAM = 1'b0;
				end
			else
				begin //we are still Reading the SRAM
				NextState = 5'b01011;  //s11 go read the next SRAM Address 
				end
			5'b10000:
				begin 
				NextState = 5'b10000; 
				Error = 1'b1;
				end
		endcase
	end //end of else
end // end of always
endmodule


	