/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module PassivesInterface (
//Inputs
Clock, 
Reset, 
EnablePassivesController,
USBCanWrite_n,

USBDataPresent_n,
DataSwTwo,
DataSwThree,
DataSwFour,
DataTemp,
Data2USB,
//Outputs 
USBWriteFifo,
TriEnableDout_n,
ReadingPassives, 
Error
);

input Clock; 
input Reset;
input EnablePassivesController;
input USBCanWrite_n;
input USBDataPresent_n;
input [7:0] DataSwTwo;
input [7:0] DataSwThree;
input [7:0] DataSwFour;
input [12:0] DataTemp;
output USBWriteFifo;
output TriEnableDout_n;
output [7:0] Data2USB; 
output ReadingPassives;
output Error;

reg USBWriteFifo;
reg [8:0] CurrentByte;
wire EnablePassivesController;
reg ReadingPassives;
reg [2:0] num_clks;
reg [2:0] NextState;
reg [2:0] CurState; 
reg TriEnableDout_n;
reg [7:0] Data2USB;
reg Error;
reg USBDataPresent_nRega;
reg USBDataPresent_nRegb;
reg USBCanWrite_nRega;
reg USBCanWrite_nRegb;

//Register asynchronous inputs
always @(posedge Clock)
begin
USBDataPresent_nRega <= USBDataPresent_n;
USBDataPresent_nRegb <= USBDataPresent_nRega;
end

always @(posedge Clock)
begin
USBCanWrite_nRega <= USBCanWrite_n;
USBCanWrite_nRegb <= USBCanWrite_nRega;
end

always @(posedge Clock)
begin
	if (Reset == 0)  //if reset is 0 then we stay in S0.
		begin
		USBWriteFifo = 1'b0;
		TriEnableDout_n = 1'b0;
		Data2USB [7:0] = 8'b00000000; 
		NextState [2:0] = 3'b000;
		CurrentByte [8:0] = 9'b000000000;
		ReadingPassives =1'b0;
		Error = 1'b0;
		end
	else //now things get exciting. . .
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			3'b000:  //s0: Wait for The Director to enable this controller
			if (EnablePassivesController == 1'b1)  
				begin
				NextState = 3'b001;
				ReadingPassives = 1'b1;
				end
			else
				begin
				USBWriteFifo = 1'b0;
				TriEnableDout_n = 1'b0;
				Data2USB [7:0] = 8'b00000000; 
				NextState [2:0] = 3'b000;
				CurrentByte [8:0] = 9'b000000000;
				ReadingPassives =1'b0;
				num_clks = 3'b000;
				Error = 1'b0;
				end	
			3'b001:  //State 1  Put the appropriate data on the Fifo data bus
			if (CurrentByte == 9'b000000000)
				begin
				NextState = 3'b010;  //s2
				Data2USB [7:0] = DataSwTwo [7:0];
				end
			else if (CurrentByte == 9'b000000001)
				begin
				NextState = 3'b010;  //s2
				Data2USB [7:0] = DataSwThree [7:0];
				end
			else if (CurrentByte == 9'b000000010)
				begin
				NextState = 3'b010;  //s2
				Data2USB [7:0] = DataSwFour [7:0];
				end
			else if (CurrentByte == 9'b000000011) 
				begin
				NextState = 3'b010;  //s2
				Data2USB [7:0] = DataTemp [7:0];
				end
			else if (CurrentByte == 9'b000000100) 
				begin
				NextState = 3'b010;  //s2
				Data2USB [4:0] = DataTemp [12:8];
				Data2USB [7:5] = 3'b000;
				end
			else if (CurrentByte <= 9'b110000000)
				begin
				NextState = 3'b010;  //s2
				Data2USB [7:0] = 8'hFF; //send 289 bytes of "FF" to fill the Rx Buffer
				end
			else   		//Go to Error State!				
				begin
				NextState = 3'b101;  //s5
				end			
			3'b010:  //State 2 We now have the data to send to the pc on USBData
			if ((USBDataPresent_nRegb == 1) && (USBCanWrite_nRegb == 1'b0))
				begin
				NextState = 3'b011;  //s3
				TriEnableDout_n = 1'b1;
				end	
			else
				begin
				NextState = 3'b010;
				end
			3'b011: //State 3 we need to de-assert USBWriteFifo after > 50ns so we'll wait 
					//five clocks to do so
				if (num_clks == 6)
					begin
					NextState = 3'b100;  //s4
					USBWriteFifo = 1'b0;   //strobing USBWriteFifo from high to low writes the data to the USB Mac
					num_clks = 1'b0;
					TriEnableDout_n = 1'b0;
					end
				else 
					begin
					NextState = 3'b011;  //s3
					num_clks = num_clks + 1'b1; 
					USBWriteFifo = 1'b1;
					end
			3'b100:  //State 4 Check to see if all data has been sent
			if (CurrentByte == 9'b101111111)
				begin
				NextState = 3'b000;  //s0 All passives data has been sent
				ReadingPassives = 1'b0;
				CurrentByte = 9'b000000000;
				end
			else
				begin
				NextState = 3'b001;  //s1 go back and write the next byte
				CurrentByte [8:0] = CurrentByte[8:0] + 1'b1;
				num_clks = num_clks +1'b1;
				end
			3'b101:  //State 5 Error state
			begin
			NextState = 3'b101;
			Error = 1'b1;
			end
			default: NextState = 3'b101;
		endcase
	end //end of else
end // end of always
endmodule


	