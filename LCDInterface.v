/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module LCDInterface (
//Inputs
Clock, 
Reset, 
EnableLCDController,
USBDataPresent_n,
DataInFromUSB,
LCDbusy, 
//Outputs 
USBReadFifo,
Data2LCD,
LCDenable,
LCDinit,
LCDrs,
WritingLCD,
//USBSendNowWkUp,
TriEnableDin_n,
Addr2LCD
);

input Clock; 
input Reset;
input EnableLCDController;
input USBDataPresent_n;
input LCDbusy;
input [7:0] DataInFromUSB;
output USBReadFifo;
output TriEnableDin_n;
output [7:0] Data2LCD; 
output LCDenable;
output LCDinit;
output LCDrs;
output WritingLCD;
output [7:0] Addr2LCD;

wire EnableLCDController;
reg WritingLCD;
wire [7:0] DataInFromUSB;
reg [7:0] Data2LCD; 
reg LCDenable;
reg LCDinit;
reg LCDrs;
//Internal Sigs
reg [2:0] num_clks;
reg [3:0] NextState;
reg [3:0] CurState; 
reg USBReadFifo;
reg TriEnableDin_n;
reg [7:0] Addr2LCD;
reg [7:0] DataForLCD;

always @(posedge Clock)
begin
	//if reset is 0 then we stay in S0. 
	if (Reset == 0)
		begin
		NextState = 4'b0000;
		USBReadFifo = 1'b0;
		TriEnableDin_n = 1'b0;
		Data2LCD [7:0]  = 8'b00000000; 
		NextState [3:0]  = 4'b0000;
		LCDenable = 1'b0;
		LCDinit = 1'b0;
		LCDrs = 1'b0;
		WritingLCD = 1'b0;
		end
	else //now things get exciting. . .  
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			4'b0000:  //State 0, wait for the LCD interface to be activated.
			if (EnableLCDController == 1'b1)
				begin
				NextState = 4'b0001;
				LCDinit = 1'b1;
				WritingLCD = 1'b1;
				end				
			else
				begin 
				NextState = 4'b0000;
				USBReadFifo = 1'b0;
				TriEnableDin_n = 1'b0;
				Data2LCD [7:0] = 8'b00000000; 
				NextState [3:0] = 4'b0000;
				LCDenable = 1'b0;
				LCDinit = 1'b0;
				LCDrs = 1'b0;
				WritingLCD = 1'b0;
				Addr2LCD [6:0] = 7'b0000000;
				end
			4'b0001:   //State 1  Wait for data to arrive in the USB MAC
			if  (USBDataPresent_n == 0)
				begin
				NextState = 4'b0010;
				end
			else 
				begin 
				NextState = 4'b0001;
				end
			4'b0010:  //State 2 After USBReadFifo goes low it takes a max of 50ns for data to be
			//present on USBData[7:0], since our clock is at 66.6 MHz, the period 
			//= ~15ns, so wait for 5 clocks before latching it
			if (num_clks == 3'b101)
				begin
				NextState = 4'b0011;
				USBReadFifo = 1'b1; 
				num_clks = 3'b000; 
				end	
			else
				begin 
				NextState = 4'b0010;
				num_clks = num_clks + 1'b1;
				TriEnableDin_n = 1'b1; //enable data bus (from FTDI to M2)
				end
			4'b0011:  //State 3 latch the data 
			begin 
			NextState = 4'b0100;
			DataForLCD = DataInFromUSB;
			end
			4'b0100:  //State 4: de-assert the USBReadFifo and we enable the tri-state bus 
			begin
			NextState = 4'b0101;
			USBReadFifo = 1'b0;
			TriEnableDin_n = 1'b0; //disable dbus (FTDI to M2)
			end
			4'b0101: //State 5 check to see whether the LCD is initialized or not
			if (Addr2LCD [6:0] == 7'b0000000) //then we need to initalize the LCD
				begin
				NextState = 4'b0110; //s6
				LCDinit = 1'b1;
				Addr2LCD [6:0] = 7'b0000000;
				Addr2LCD [7] = 1'b1;
				end
			else
				begin
				NextState = 4'b0111; //s7
				end
		    4'b0110:  //State 6 Ensure that the LCD has asserted busy, set the first LCD address and then move to s6 to wait until it's finished with initialization
			if (LCDbusy == 1)		
				begin
				NextState = 4'b0111; //s7
				LCDinit = 1'b0;
				end
			else
				begin
				NextState = 4'b0110;  //s6
				end	
            4'b0111:  //State 7 wait for initialization to be complete 
			if (LCDbusy == 0)
				begin
				NextState = 4'b1000; //s8
				Data2LCD[7:0]=Addr2LCD[7:0];
				LCDrs = 1'b0;
				LCDinit = 1'b0;
				end
			else
				begin
				NextState= 4'b0111; //s7
				LCDinit = 1'b0;
				end
			4'b1000:  //State 8 drop LCD Enable to latch the address into the LCD
			begin
			NextState = 4'b1001;  //s9
			LCDenable = 1'b1;
			end
			4'b1001:  //State 9 pull LCD enable back inactive & Prepare next addr
			begin
			NextState = 4'b1010; //s10
			LCDenable = 1'b0;
			end
			4'b1010:  //State 10 we wait for the LCD to assert LCDbusy and put data on the lcd dbus
			if (LCDbusy == 1)
				begin
				NextState = 4'b1011;  //s11
				Data2LCD[7:0] = DataForLCD[7:0];		
				LCDrs = 1'b1;
				end	
			else
				begin
				NextState = 4'b1010;  //s10
				end
			4'b1011:  //State 11 wait for LCD to be done with address, and then clock in the data
			if (LCDbusy == 0)    
				begin
				NextState = 4'b1100;  //s12
				LCDenable = 1'b1;
				end				
			else
				begin
				NextState = 4'b1011;  //s11
				end
			4'b1100:  //State 12 de-assert LCDenable
			begin
			NextState = 4'b1101;  //s13
			LCDenable = 1'b0; 
			end
			4'b1101: //State 13 wait for LCDbusy signal to go high
			if (LCDbusy == 1) 
				begin
				NextState = 4'b1110;  //s14
				end
			else
				begin
				NextState = 4'b1101;  //s13
				end
			4'b1110:  //State 14 Wait for LCD to be done with the data
			if (LCDbusy == 0) 
				begin
				NextState = 4'b1111;  //s15
				end			
			else
				begin
				NextState = 4'b1110;  //s14
				end
			4'b1111:  //State 15  See if all the chars have been written to the LCD or not
			if (Addr2LCD == 8'b11001111)  
				begin                     
				NextState = 4'b0000;  //s0
				Addr2LCD = 8'b00000000;
				WritingLCD = 1'b0;
				end
			else if (Addr2LCD == 8'b10001111) 
				begin
				NextState = 4'b0001; //s1
				Addr2LCD = 8'b11000000;
				end 
			else 
				begin
				NextState = 4'b0001; //s1
				Addr2LCD = Addr2LCD + 1'b1;
				end
		endcase
	end //end of else
end // end of always
endmodule


	