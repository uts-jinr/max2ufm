/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module UFMInterface (
//Inputs
Clock, 
Reset,
UFM_Mode,
nbusy,
UFMdataValid,
USBDataPresent_n,
USBCanWrite_n,             // TXEn - output signal for FT245BL
UFMdout,
//Outputs
UFMnread,
Addr2UFM,
Data2USB,
TriEnableDout_n,
USBWriteFifo,              // It's a "WR" = "WRITE"  Input signal for FT245BL
ReadingUFM,
WritingUFM,
UFMdin,
UFMn_erase,
UFMnwrite,
Error
);

input Clock; 
input Reset;
input [1:0] UFM_Mode;
input nbusy;
input UFMdataValid;
input USBDataPresent_n;
input USBCanWrite_n;
input [15:0] UFMdout;

output UFMnread;
output [8:0] Addr2UFM;
output [7:0] Data2USB;
output TriEnableDout_n;
output USBWriteFifo;
output ReadingUFM;
output WritingUFM;
output [15:0] UFMdin;
output UFMnwrite;
output UFMn_erase;
output Error;

reg [4:0] num_clks;
reg [4:0] NextState;
//reg [4:0] CurState; 
reg ReadingUFM;
reg WritingUFM;
wire [1:0] UFM_Mode;
reg TriEnableDout_n;
reg [7:0] Data2USB;
reg UFMnread;
reg UFMnwrite;
reg UFMn_erase;
reg [8:0] Addr2UFM;
wire [15:0] UFMdout;
reg  [15:0] UFMdin;
reg USBWriteFifo;
reg Error;
reg UFM_DataVal_rega;
reg UFM_DataVal_regb;
reg USBDataPresent_nRega;
reg USBDataPresent_nRegb;
reg USBCanWrite_nRega;
reg USBCanWrite_nRegb;

reg sector_address;



parameter write_ufm_00 = 5'b10000, write_ufm_01 = write_ufm_00 + 1, erase_ufm_00 = write_ufm_01 + 1, erase_ufm_01 = erase_ufm_00 + 1;
parameter erase_ufm_02 = erase_ufm_01 + 1;
parameter write_ufm_02 = erase_ufm_02 + 1;
parameter write_ufm_03 = write_ufm_02 + 1;

parameter write_ufm_04 = write_ufm_03 + 1;
parameter write_ufm_05 = write_ufm_04 + 1;
parameter write_ufm_06 = write_ufm_05 + 1;
parameter write_ufm_07 = write_ufm_06 + 1;

/*
parameter write_ufm_00 = 5'b10000; 
parameter write_ufm_01 = 5'b10001; 
parameter erase_ufm_00 = 5'b10010; 
parameter erase_ufm_01 = 5'b10011;
parameter erase_ufm_02 = 5'b10100;
parameter write_ufm_02 = 5'b10101;
parameter write_ufm_03 = 5'b10110;
*/
//Register all asynchronous inputs
always @(posedge Clock)
begin
UFM_DataVal_rega <= UFMdataValid;
UFM_DataVal_regb <= UFM_DataVal_rega;
end

always @(posedge Clock)
begin
USBDataPresent_nRega <= USBDataPresent_n;
USBDataPresent_nRegb <= USBDataPresent_nRega;
end

always @(posedge Clock)
begin
USBCanWrite_nRega <= USBCanWrite_n;         // It's the "USB_TxEn_C5" signal
USBCanWrite_nRegb <= USBCanWrite_nRega;
end


// a high-to-low transition of a mode signal (nREAD, nWRITE, or nERASE)
// requires a minimum of 420 ns of hold time before the instruction signal can be pulled high again.
// since our clock is at 66.6 MHz, the period = ~15ns, 
// And as nread signal should be 30 cycles long


always @(posedge Clock)
begin
	if (Reset == 0) //if reset is 0 then we stay in S0.
		begin
		NextState = 5'b00000;
		UFMnread = 1'b1;
		UFMnwrite = 1'b1;
		UFMn_erase = 1'b1;
		Addr2UFM [8:0] = 9'b000000000;
		Data2USB [ 7:0] = 8'b00000000;
		TriEnableDout_n = 1'b0;
		USBWriteFifo = 1'b0;   
		num_clks = 5'b00000;
		ReadingUFM = 1'b0;
		WritingUFM = 1'b0;
		sector_address = 1'b0;
		Error = 1'b0;
		end
	else //now things get exciting. . . 
		begin
		case (NextState)
			5'b00000:
			if (UFM_Mode[1:0] == 2'b01)    // Режим чтения UFM. Чтение всей UFM занимает 5.6мс, включая отправку каждого байта в ПК и считывание его там программой.
				begin
				NextState = 5'b00001;
				ReadingUFM = 1'b1;
				WritingUFM = 1'b0;
				end
			else if (UFM_Mode[1:0] == 2'b10) // Режим записи в UFM
			    begin
				 NextState = write_ufm_00;     // Операция  ERASE занимает 450ms
				 WritingUFM = 1'b1;            // Tell 'TheDirector' what am i doing
				 ReadingUFM = 1'b0;
				 UFMdin[15:0] = 16'H7777;
				 
				 end
		   else	if (UFM_Mode[1:0] == 2'b00) // Все UFM операции завершены, ждем начала новой операции, т.е. когда станет (UFM_Mode[1:0] != 2'b00)
				begin
				NextState = 5'b00000;
				UFMnread = 1'b1;
    			UFMnwrite = 1'b1;
	      	UFMn_erase = 1'b1;

				Addr2UFM [8:0] = 9'b000000000;
				Data2USB [ 7:0] = 8'b00000000;
				TriEnableDout_n = 1'b0;
				USBWriteFifo = 1'b0;   
				num_clks = 5'b00000;
				ReadingUFM = 1'b0;
				WritingUFM = 1'b0;
				
				Error = 1'b0;
				end	
			5'b00001:  //State 1  Reading the UFM
			if (nbusy == 1'b1) //Check to see if the UFM is busy
				begin
				NextState = 5'b00010;  //s2 NO, not busy
				UFMnread = 1'b0;       // Strobing read from high to low  
				end
			else
				begin
				NextState = 5'b00001; //s1
				end
				
	// Вот тут отсечка, чтобы сбросился текущий			(UFM_DataVal_regb == 1'b0)
	// И потом долго ждем, чтобы     (UFM_DataVal_regb == 1'b1)
	
			5'b00010:  //State 2 Wait for the ufm to pull ufmbusy low & dataValid high
			if ((nbusy == 1'b0) && (UFM_DataVal_regb == 1'b0))
				begin
				NextState = 5'b00011; //s3
				end
			else
				begin
				NextState = 5'b00010;  //s2
				end 			
				
//++++++++++++++ Вот тут проходит почти 6 мкс ++++++++++++++++				
				
			5'b00011:  //State 3 wait for the data to be valid and then put it on the MAC input bus
			if (UFM_DataVal_regb == 1'b1)
				begin
				NextState = 5'b00100;  //s4
				Data2USB [7:0] = UFMdout[15:8]; // Это 1-я порция данных, 8 бит из 16
				UFMnread = 1'b1; // Убрали чтение UFM, т.е. данная порция данных считана.
   			
				UFMnwrite = 1'b1;  //Вдруг поможет и nbusy станет == '1'
	      	UFMn_erase = 1'b1;

				end
			else
				begin 
				NextState = 5'b00011;
				end
				
//++++++++++++++++ И вот тут почему-то UFMnread == '1', Но при этом nbusy == 1'b0 и UFM_DataVal_regb == 1'b0
//++++++++++++++++ т.е. операцию с UFM закончили а эти сигналы все равно в активном состоянии.	
				
			5'b00100: //State 4 see if the MAC is ready for new portion of data
			          // Нет данных для считывания из UDB [FTDI] и Можно передавать в FTDI, TXEn=='0'
			if ((USBDataPresent_nRegb == 1) && (USBCanWrite_nRegb == 1'b0))
				begin
				TriEnableDout_n = 1'b1;// Разрешаем шину данных USB наружу
				NextState = 5'b00101;  //s5
				end	
			else
				begin
				NextState = 5'b00100; //s4
				end
				
			// Создаем ворота "WR"	для 1 порции 8 байт
			5'b00101: //State 5 we need  (to  keep  USBWriteFifo high at least 50ns ) to de-assert USBWriteFifo after > 50ns so we'll wait 
					//five clocks to do so
				if (num_clks == 7)
					begin
					NextState = 5'b00110;  //s6
					USBWriteFifo = 1'b0;   //strobing USBWriteFifo from high to low writes the data to the USB Mac
					num_clks = 5'b00000;
					end
				else 
					begin
					NextState = 5'b00101;  //s5
					num_clks = num_clks + 1'b1; 
					USBWriteFifo = 1'b1;
					end
			5'b00110:  //State 6 Put the second half of the data from UFM on the MAC input bus
			begin
			NextState = 5'b00111;  //s7			
			Data2USB[7:0] = UFMdout[7:0]; 
			end
			5'b00111: //State 7 Check to see if the MAC is ready for data
			if ((USBDataPresent_nRegb == 1) && (USBCanWrite_nRegb == 1'b0))   //"TXEn" When high, do not write data into the FIFO.
				begin
				NextState = 5'b01000;  //s8
				TriEnableDout_n = 1'b1;
				end	
			else
				begin
				NextState = 5'b00111; //s7
				end

				// Создаем ворота "WR"	для 2 порции 8 байт
				5'b01000: //State 8 we need (to  keep  USBWriteFifo high at least 50ns ) to de-assert USBWriteFifo after > 50ns so we'll wait 
					//five clocks to do so
			if (num_clks == 7)
				begin
				NextState = 5'b01001;  //s9
				USBWriteFifo = 1'b0;   //strobing USBWriteFifo from high to low writes the data to the USB Mac
				num_clks = 5'b00000;
				Addr2UFM [8:0] <= Addr2UFM [8:0] + 1'b1; //increment the UFM address
				end
			else 
				begin
				NextState = 5'b01000;  //s8
				num_clks = num_clks + 1'b1; 
				USBWriteFifo = 1'b1;
				end
	
   	5'b01001:  //State 9 Check to see if we've read 384 bytes of UFM data (384 bytes = 192 address locations)
			          //9'h200 it's the last address

				
	     if ((Addr2UFM [8:0] == 9'h0c0) || (Addr2UFM [8:0] == 9'h180)  || (Addr2UFM [8:0] == 9'h200))  //  Check to see if we've read 384/768 bytes of UFM data (384 bytes = 192 address locations, 768 bytes = 384 loacations respectively)
	         begin
	         // So we should wait while 384 bytes are transferred to PC
				// i.e. the signal "TXEn" == '0'
				NextState = 5'b01011; //go to wait_for_TXEn	
	         end 

	     else NextState = 5'b01010;
		  
  
		  5'b01010:  // 
		  
		  if (Addr2UFM [8:0] == 9'h200)  //9'h0C0
			/*
			   Вот тут засада в том, что у FT245BL 384 Byte FIFO Tx buffer / 128 Byte FIFO Rx Buffer
				Т.е. размер буфера 384 байт, а мы передаем 512 ячеек по 2 байта.
				Т.е. прога то это кушает, но после этого зависает.
				Короче, считываем за 3 захода :
				 - 1-е 384 байта, это 192(9'h0C0) ячейки по 2 байта
				 - 2-е 384
				 - Завершающие 256 байт
				
			
			*/
				begin
				//NextState = 5'b00000; //so
				NextState = write_ufm_07;
			//	Addr2UFM [8:0] = 9'b000000000;
				ReadingUFM = 1'b0;
				TriEnableDout_n = 1'b0; // Открываем шину USB на выдачу из fpga
			//	USBWriteFifo = 1'b0;    // "WR" не активен
				end
			else  	//then we have more data in UFM, back to s1
				begin
				NextState = 5'b00001; //s1
				TriEnableDout_n = 1'b0;  // Держим USB шину закрытой на выдачу из UFM
				USBWriteFifo = 1'b0;
				end
				
				
		   5'b01011:  // Ждем, пока FIFO буфер FT245BL будет передан в USB
        
				if ((USBCanWrite_nRegb == 1'b0)) 
				   begin
					NextState = 5'b01010; //go to next
					end
			   else NextState = 5'b01011; //wait  //"TXEn" When high, do not write data into the FIFO.
		
				
				
			
			5'b11111:  //State 10 Error State
			begin
			NextState = 5'b11111;
			Error = 1'b1;
			end 
			
//++++++++++++++++++++++++++++ Блок кода для записи UFM	

// сначала делаем ERASE		

/*
During the read/write mode, a high-to-low transition of a mode signal (nREAD, nWRITE, or nERASE)
requires a minimum of 420 ns of hold time before the instruction signal can be pulled high again. The address
register and data input must be held for at least 420 ns once the mode signal is asserted low.
*/



/*
A write operation is only possible on an erased UFM block or word location. The
UFM block differs from serial EEPROMs, requiring an erase operation prior to writing
new data in the UFM block. A special erase sequence is required, 

 When using read/write mode, a sector or full memory erase operation is
required before writing new data into any location that previously contained data. 

Erase time 500000 ns
oscillator frequency 5.56 MHz, period = 180ns


The ERASE signal initiates an erase sequence to erase one sector of the UFM. The data
register is not needed to perform an erase sequence. To indicate the sector of the UFM
to be erased, the MSB of the address register should be loaded with 0 to erase the
UFM sector 0, or 1 to erase the UFM sector 1 (Figure 9–2 on page 9–5). On a rising
edge of the ERASE signal, the memory sector indicated by the MSB of the address
register will be erased. The BUSY signal is asserted until the erase sequence is
completed. The address register should not be modified until the BUSY signal is deasserted
to prevent the content of the flash from being corrupted. This ERASE signal
will be ignored when the BUSY signal is asserted. Figure 9–10 illustrates the UFM
waveforms during erase mode.

*/

/*
Ставим в 1'b0 сигнал nErase, ждем 420нс.
Дожидаемся сигнала nBUSY '0', после этого можно выполнять операции.
После окончания операции сигнал nBUSY вернется в '1'



*/
  /*       write_ufm:
			if (erase_passed) 
			    begin
				 sector_address = 1'b0;    // ОБнуляем счетчик секторов
				 NextState = write_ufm_02; // Переходим к записи в UFM
			    end
			else
			    begin
				 NextState = write_ufm_00; // Начинаем, продолжаем erase UFM
				 end
*/
//+++++++++++++++++++++++++++++++  ERASE at first


                       // Убеждаемся, что нет сигнала 'BUSY', т.е. с UFM не работаем
			write_ufm_00: //State 1 wait for DEassert 'BUSY' signal.  Writing the UFM
			if (nbusy == 1'b1) //Check to see if the UFM is busy
				begin
				NextState = write_ufm_01;  //s2 NO, not busy
				UFMn_erase = 1'b0; // but first we need to erase the UFM
				UFMnwrite = 1'b1;
				end
			else
				begin
				NextState = write_ufm_00; //it's busy so go back to wait
				end
	                   // Дожидаемся появления сигнала 'BUSY' в ответ на 'UFMn_erase = 1'b0;', это значит мы его заняли.
			write_ufm_01://State 2 Wait for the ufm to pull ufmbusy low 
			
			if ((nbusy == 1'b0) && (UFMn_erase == 1'b0))
				begin
       		 num_clks = 5'h00;
		//		  if (sector_address == 1'b0) Addr2UFM [8:0] = 9'b000000000;
		//		  if (sector_address == 1'b1) Addr2UFM [8:0] = 9'b100000000;
				NextState = erase_ufm_00; //s3
				end
			else
				begin
				NextState = write_ufm_01;  //Ждем дальше
				end 			
				
		  erase_ufm_00: //Ждем в этом статусе 420ns( по осциллографу это ...нс ???), чтобы снять сигнал nERASE // пока не будет снят сигнал nbusy, что означает "память стерта"
        if (num_clks == 5'h1E) // Ждем 30 циклов по 15 нс, поднимаем nERASE
		     begin
			  UFMn_erase = 1'b1;         // Убрали nERASE
			  NextState = erase_ufm_01;
			  end 
 	     else
		    begin
			 num_clks = num_clks + 1'b1;
			 NextState = erase_ufm_00;
			 end
		  erase_ufm_01: // Теперь ждем сброса nbusy, т.е. nbusy == 1'b1, это будет окончание стирания сектора //The BUSY signal asserts until the erase sequence is completed.
		  if (nbusy == 1'b1) //The new PROGRAM or ERASE instruction will not be executed until the BUSY signal is deasserted. 
	     begin
           NextState = erase_ufm_02;
		  end
			else  NextState = erase_ufm_01;
			  
		  erase_ufm_02: // ДОждались сброса BUSY, проверяем Если уже оба сектора erase, то идем на writing, либо старший сектор erase ВЫСТАВЛЯЕМ СТАРШИЙ СЕКТОР НА УДАЛЕНИЕ И снова все шаги erase
		    if (sector_address )      //== 1'b1       
			 begin 
		//	 erase_passed = 1'b1;    // Поднимаем флаг окончания erase
		//    NextState = write_ufm;  // Переходим в начало развилки erase OR write ?
		    
			 sector_address = 1'b0;    // ОБнуляем счетчик секторов
			 Addr2UFM [8:0] = 9'b000000000;   //Выставляем UFM0 сектор, ставим "указатель" на начало.
			//	  if (sector_address == 1'b1) Addr2UFM [8:0] = 9'b100000000;

			 NextState = write_ufm_02; // Переходим к записи в UFM
		//	 WritingUFM = 1'b0;    // Снимаем флаг "ИДЕТ ЗАПИСЬ"
			 WritingUFM = 1'b1;    // Все еще держим флаг "ИДЕТ ЗАПИСЬ"
			// NextState = 5'b00000; // Завершили ERASE, Переходим в начало
			 //NextState = write_ufm_02;  //Завершили ERASE,  Переходим на запись новых данных
			 end
			 
		  else if (!sector_address) // == 1'b0
		    begin // 
			 WritingUFM = 1'b1;    // Все еще держим флаг "ИДЕТ ЗАПИСЬ"
			 sector_address = 1'b1;         // ВЫСТАВЛЯЕМ СТАРШИЙ СЕКТОР НА УДАЛЕНИЕ		
	       Addr2UFM [8:0] = 9'b100000000; //Выставляем UFM1 сектор

			 NextState = write_ufm_00;      // Идем на стирание 2 сектора
			 end
			

//+++++++++++++++++++++++++++++++ Now Writing sectores erased before 

/*
Это циклическая запись одинаковых байт 16'H7777 во все ячейки UFM.
Всего адресов 512  - if (Addr2UFM [8:0] == 9'h200) 
*/

// А вот теперь приступим к записи.

			              // write to UFM cell begining
		write_ufm_02:    // дожидаемся 'nBUSY', Выставляем сигнал nWRITE
		begin 
		WritingUFM = 1'b1;    // Все еще держим флаг "ИДЕТ ЗАПИСЬ"
		
		if (nbusy == 1'b1) //Check to see if the UFM is busy	
	       begin
			 	UFMn_erase = 1'b1; // 
				UFMnwrite = 1'b0;  // Выставили сигнал nWRITE,
            NextState = write_ufm_03;
			 
			 end 
		else NextState = write_ufm_02;
		end //write_ufm_02:
			 
	   write_ufm_03:  //  Дожидаемся 'nBUSY', Выставляем адрес и данные. 
      if ((nbusy == 1'b0) && (UFMnwrite == 1'b0))   
         begin
			num_clks = 5'h00;
//       UFMdin[15:0] = 16'H7777;  //Данные уже выставлены, тут пропускаем        
//       Адрес тоже выставлен ранее 
			NextState = write_ufm_04;
			end
	   else NextState = write_ufm_03;
		
	   write_ufm_04:  	
		 if (num_clks == 5'h1E) // Ждем 30 циклов по 15 нс, поднимаем nWRITE, 600нс - это 40 циклов, т.е. 5'h28
      // Данные, адрес выставили, убираем 
		     begin
			  UFMnwrite = 1'b1;         // Убрали UFMnwrite, команда распознана
			  NextState = write_ufm_05; // Идем дальше
			  end 

 	     else 
		    begin
			 num_clks = num_clks + 1'b1;
          NextState = write_ufm_04;    // Ждем
			 
			 end
			 
		write_ufm_05:	 // дожидаемся сброса 'nBUSY', Инкрементируем адрес, выставляем следующий адрес.
		if (nbusy == 1'b1)
		    begin
			  Addr2UFM [8:0] = Addr2UFM [8:0] + 1'b1; //increment the UFM address
			  NextState = write_ufm_06;
		    end
	   else NextState = write_ufm_05;
		
		write_ufm_06: // Проверяем, мы у финиша ?
		if (Addr2UFM [8:0] == 9'h200) 
		   begin
			WritingUFM = 1'b0;    // Снимаем флаг "ИДЕТ ЗАПИСЬ"
			NextState = write_ufm_07; // Идем на ожидание сброса UFM_Mode
			end
		else
		   begin
			//then we have more data, back to writing_ufm cell cycle
         NextState = write_ufm_02;
			
			end
	
	   write_ufm_07:	 // Дожидаемся сброса UFM_Mode
	   if (UFM_Mode[1:0] == 2'b00) // Пришел ответ от 'Data Director',  т.е. (UFM_Mode[1:0] == 2'b00), Все UFM операции завершены, переходим в ожидание начала новой операции,
		   begin
			NextState = 5'b00000; // Завершили nWrite, Переходим в начало, где все зануляется.
		   end
		else
		   begin
			NextState = write_ufm_07; // ЖДем 
		   end
	
	default: NextState = 5'b00000;  //State 10 5'b01010;
		endcase
	end //end of else
end // end of always
endmodule


	