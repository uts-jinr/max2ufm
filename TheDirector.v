/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */


/* 

Принимает данные от USB, в зависимости от данных запускает/активирует соответствующие модули,
- ждет ответные сигналы от этих модулей "занято", "освободился".

- QuaterClock period is 60ns

*/

module TheDirector (
//Inputs
Clock, 
Reset, 
USBCanWrite_n,
USBDataPresent_n,
SwitchTwo, 
SwitchThree,
SwitchFour, 
DataInFromUSB,
WritingLCD, 
WritingSRAM,
ReadingSRAM, 
ReadingPassives,
ReadingUFM,
WritingUFM, //30.10.2018
//Outputs 
USBReadFifo,
USBWriteFifo, 
TriEnableDin_n,
TriEnableDout_n,
DataDirector,
//Data2Control, 
EnablePassives,
EnableLCD,
EnableSRAMWrite,
EnableSRAMRead,
UFM_MODE,
LEDs,
Error 
);

input Clock; 
input Reset;
input USBCanWrite_n;
input USBDataPresent_n;
input SwitchTwo;
input SwitchThree;
input SwitchFour;
input [7:0] DataInFromUSB;
input WritingLCD; 
input WritingSRAM;
input ReadingSRAM; 
input ReadingPassives;
input ReadingUFM; 
input WritingUFM;    //30.10.2018
output USBReadFifo;
output USBWriteFifo;
output TriEnableDin_n;
output TriEnableDout_n;
output [3:0] LEDs;
output EnablePassives;
output EnableLCD;
output EnableSRAMWrite;
output EnableSRAMRead;
output [1:0] UFM_MODE;
output Error;
output [3:0] DataDirector;

wire [7:0] DataInFromUSB;
reg USBWriteFifo;
reg [3:0] DataDirector;
reg EnablePassives;
reg EnableLCD;
reg EnableSRAMWrite;
reg EnableSRAMRead;
reg [1:0] UFM_MODE;
wire WritingLCD; 
wire WritingSRAM;
wire ReadingSRAM; 
wire ReadingPassives;
wire ReadingUFM; 
wire WritingUFM;    //30.10.2018
reg [3:0] LEDs;
//Internal Sigs
reg [2:0] num_clks;
reg [3:0] NextState;
reg [3:0] CurState; 
reg USBReadFifo;
reg TriEnableDin_n;
reg TriEnableDout_n;
reg SwitchTwo_rega;
reg SwitchTwo_regb;
reg SwitchThree_rega;
reg SwitchThree_regb;
reg SwitchFour_rega;
reg SwitchFour_regb;
reg USBDataPresent_nRega;
reg USBDataPresent_nRegb;
reg Error;


// Надо задефайнить шаг "State 7 ensure that the activated controller has asserted its busy signal"
// Чтобы менять код этого состояния только в 1 месте кода при последующих добавлениях state-ов

parameter busy_set = 4'b1101;
parameter error_state = 4'b1111;
parameter wait_state = 4'b1110;

parameter write_UFM = 4'b0111;


//First we'll buffer all asynchronous inputs with two registers. 
always @(posedge Clock)
begin
SwitchTwo_rega <= SwitchTwo;
SwitchTwo_regb <= SwitchTwo_rega;
end

always @(posedge Clock)
begin
SwitchThree_rega <= SwitchThree;
SwitchThree_regb <= SwitchThree_rega;
end

always @(posedge Clock)
begin
SwitchFour_rega <= SwitchFour;
SwitchFour_regb <= SwitchFour_rega;
end

always @(posedge Clock)
begin
USBDataPresent_nRega <= USBDataPresent_n;
USBDataPresent_nRegb <= USBDataPresent_nRega;
end


always @(posedge Clock)
begin
	//if reset is 0 then we stay in State 0
	if (Reset == 0)
		begin
		USBReadFifo = 1'b0;
		USBWriteFifo = 1'b0;
		TriEnableDin_n = 1'b0;
		TriEnableDout_n = 1'b0;
		NextState [3:0] = 4'b0000;
		LEDs [3:0] = 4'b0000;
 		DataDirector [3:0]= 4'b0000;
		EnablePassives = 1'b0;
		EnableLCD = 1'b0;
		EnableSRAMWrite = 1'b0;
		EnableSRAMRead = 1'b0;
		UFM_MODE[1:0] = 2'b00;
		Error = 1'b0;
		end
	else //now things get exciting. . .  
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			4'b0000:  //State 0 When a pushbutton is depressed, read the passives (after debouncing the switch)
			begin //just to be safe, de-assert all outputs, except the LED's.
			USBReadFifo = 1'b0;
			USBWriteFifo = 1'b0;
			TriEnableDin_n = 1'b0;
			TriEnableDout_n = 1'b0;
			NextState [3:0] = 4'b0000;
		 	DataDirector [3:0]= 4'b0000;
			EnablePassives = 1'b0;
			EnableLCD = 1'b0;
			EnableSRAMWrite = 1'b0;
			EnableSRAMRead = 1'b0;
			UFM_MODE[1:0] = 2'b00;
			num_clks = 3'b000;
			Error = 1'b0;
			if ((SwitchTwo_regb == 1'b0) || (SwitchThree_regb == 1'b0) || (SwitchFour_regb == 1'b0))
				begin
				NextState = 4'b0001;
				end				
			else
				begin 
				NextState = 4'b0011;
				end
			end
			4'b0001: //State 1, wait for the depressed switch to be de-asserted
			if ((SwitchTwo_regb == 1'b1) && (SwitchThree_regb == 1'b1) && (SwitchFour_regb == 1'b1))
				begin
				NextState = 4'b0010;
				end
			else
				begin
				NextState = 4'b0001;
				end
			4'b0010:  //State 2, set the Data Director value and enable PassivesInterface
				begin
				NextState = busy_set; //s7 On to wait for passives to complete! 
				DataDirector = 4'b0001;
				EnablePassives = 1'b1;
				end
			4'b0011:   //State 3 check for data from the USB MAC 
			if  (USBDataPresent_n == 0) // YES ! data present  OR “receive buffer not empty yet”
				begin
				NextState = 4'b0100;
				end
			else 
				begin 
				NextState = 4'b0000;
				end
			4'b0100:  //State 4 After USBReadFifo goes from low to high it takes a max of 50ns for data to be
			//present on USBData[7:0], since our clock is at [66.6 MHz  thats's wrong]   16.7MHz, the period 
			//= ~60ns [~15ns], so we'll wait for it for 5 clocks before latching it 
			if (num_clks == 7)
				begin
				NextState = 4'b0101; //s5
				num_clks = 3'b000; 
				end	
			else
				begin 
				NextState = 4'b0100;  //s4
				num_clks = num_clks + 1'b1;
				USBReadFifo = 1'b1;        //Fetches the next FIFO Data Byte (if available) from the Receive FIFO Buffer when RD# goes from low to high.
				TriEnableDin_n = 1'b1; //enable data bus (from FTDI to M2)
				end
			4'b0101:  //State 5 latch the data from the USB MAC FIFO
			begin 
			NextState = 4'b0110;  //s6
			DataDirector [3:0] = DataInFromUSB [3:0];
			end
			4'b0110:  //State 6 de-assert the USBReadFifo and enable the Tristate Bus
			begin
			USBReadFifo = 1'b0;    // Enables Current FIFO Data Byte on D0..D7 when low.
			TriEnableDin_n = 1'b0; //disable dbus (FTDI to M2)
			
			//The first byte that DataDirector reads is a control byte indicating which function the user
			//has requested.  The value of this byte dictates which module is enabled next. 
			if (DataDirector == 4'b0010) //Read the UFM
				begin
				NextState = busy_set;  //s7
				UFM_MODE[1:0] = 2'b01; // Read Mode for "UFM Interface"
				end
			else if (DataDirector == 4'b0011) //Read the SRAM
				begin 
				NextState = busy_set;  //s7
				EnableSRAMRead = 1'b1;
				end
			else if (DataDirector == 4'b0100) //Writing to the SRAM
				begin
				NextState = busy_set;  //s7
				EnableSRAMWrite = 1'b1;
				end
			else if (DataDirector == 4'b0101) //Write to the LCD
				begin
				NextState = busy_set; //s7
				EnableLCD = 1'b1;
				end
			else if (DataDirector == 4'b0110) //Light the LED's
				begin
				NextState = 4'b0000;  //s0
				LEDs = DataInFromUSB[7:4];
				DataDirector = 4'b0000;         // Так мы закрываем  USBDataOutMux на выход, ничего не выдаем.
				end			
//++++++++++++++++++++++++ Делаем вставочку для WritingUFM +++++++++++++ write_UFM  
				
			else if(DataDirector == 4'b0111) //Write to the UFM
			   begin
				NextState = busy_set;  //busy state
				UFM_MODE[1:0] = 2'b10; // Write UFM Mode				
				end
				
//			write_UFM:
//				NextState = busy_set;   //s7
			else // Перебрали все варианты
				NextState = error_state;  //s9 Error error_state

      end // of 4'b0110:
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			busy_set:  //State 7 ensure that the activated controller has asserted its busy signal
//			if ((WritingLCD == 1) || (WritingSRAM ==1) || (ReadingSRAM ==1) || (ReadingPassives == 1) || (ReadingUFM == 1) || (WritingUFM == 1))	
			if ((ReadingPassives == 1) || (ReadingUFM == 1) || (WritingUFM == 1))	 //(ReadingPassives == 1) ||
				
				begin
				NextState = wait_state; //s8
				EnablePassives = 1'b0;
				EnableLCD = 1'b0;
				EnableSRAMWrite = 1'b0;
				EnableSRAMRead = 1'b0;
				UFM_MODE[1:0] = 2'b00; //Лушче здесь занулять, чтобы в UFMInterface по окончании операции чтения/записи всей UFM был нулевой режим
				end
			else
				begin
				NextState = busy_set;  //s7 
				end	

   		wait_state:  //State 8 wait for the activated Controller to finish 
//			if ((WritingLCD == 0) && (WritingSRAM ==0) && (ReadingSRAM ==0) && (ReadingPassives == 0)&& (ReadingUFM == 0)&& (WritingUFM == 0))	
         if ((ReadingPassives == 0)&& (ReadingUFM == 0)&& (WritingUFM == 0))	
			begin
				NextState = 4'b0000; //s0
				DataDirector = 4'b0010;
	//			UFM_MODE[1:0] = 2'b00; // Вот тут зануляем mode, т.к. текущую операцию закончили.
	         TriEnableDin_n = 1'b0; // enable the Tristate Bus, т.е. закрываем шину USB на вход 
				end
			else
				begin
				NextState=wait_state; //s8
				end
error_state: //State 9 Error State
				begin
				NextState = error_state;
				Error = 1'b1;
				LEDs[0] = 1'b1;
				end
			default: NextState = error_state;
		endcase
	end //end of else
end // end of always
endmodule


	