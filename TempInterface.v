/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module TempInterface (
Clock,
Reset,
SCK,
SerialDataIn,
CS_n,
CurrentTemperature,
regParallelDataOut,
DataValid
);

input Clock;
input Reset;
input SerialDataIn;

output SCK;
output CS_n;
output [15:0] regParallelDataOut;
output [15:0] CurrentTemperature;
output DataValid;

reg [15:0] CurrentTemperature;
reg [15:0] regParallelDataOut;
reg DataValid;
reg CS_n;
reg [2:0] NextState;
reg [2:0] CurState;
reg [3:0] NumBits;
reg [23:0] MyCounter;
reg SCK; 

always @(posedge Clock)
begin
	if (Reset == 0) //if reset is 0 then we stay in S0.
		begin
		NextState = 3'b000;
		DataValid = 1'b0;
		CS_n = 1'b0;
		end
	else //now things get exciting. . .  
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			3'b000:  	//State 0, to begin a new conversion, hold CS high for > 320ms
						//slow clock has a period of 12,000 ns
						//320ms/30ns = 10,666,666 clocks.  Rounding up, CS needs to be held low for 1010 0010 1100 0011 1111 1000 (10,667,000) clocks. 
			if (MyCounter == 24'hA2C3F8)
				begin	
				NextState = 3'b001;							
				CS_n = 1'b0;  //pull CS low and read temp data
				end
			else
				begin
				NextState = 3'b000;
				MyCounter = MyCounter + 1'b1;
				CS_n = 1'b1;
				SCK = 1'b1;
				end
			3'b001:  //State 1 Pull SCK high for > 100 ns (30ns clock => 4 clocks)
			if (MyCounter == 24'h000003)
				begin
				NextState = 3'b010;
				SCK  = 1'b0; 
				MyCounter = 24'h000000;
				end
			else
				begin
				NextState = 3'b001;
				MyCounter = MyCounter + 1'b1;
				SCK  = 1'b1; 
				end
			3'b010:  //State 2 Pulling SCK low presents data on the SerialDataIn input
			if (MyCounter == 24'h000003)
				begin
				regParallelDataOut = regParallelDataOut << 1;
				NextState = 3'b011;
				MyCounter = 24'h000000;
				end
			else
				begin
				NextState = 3'b010;
				MyCounter = MyCounter + 1'b1;
				end
			3'b011:  //State 3 add the new bit to the shift register and pull SCK high 
				begin
				regParallelDataOut[0] = SerialDataIn;
				NextState = 3'b100;
				NumBits = NumBits + 1'b1;
				end
			3'b100:  //State 4 see if we're done with a conversion
			if (NumBits == 4'b0000)
				begin
				NextState = 3'b101; //update the output bus
				CS_n = 1'b0;
				DataValid = 1'b0;
				NumBits = 4'b0000;
				end
			else
				begin
				NextState = 3'b001;
				end
			3'b101: //State 4 Update the output 
			begin
			CurrentTemperature = regParallelDataOut;
			NextState = 3'b000;  //go start a new conversion
			DataValid = 1'b1;
			end
		endcase
	end //end of else
end // end of always 
endmodule


	