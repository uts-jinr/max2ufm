/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */


module LEDFlasher (
Clock, 
QuarterClock,
Enable,
USBDataPres,
FlashLED,
ErrorUSB_read
);

input Clock; 
input QuarterClock;
input Enable;
input USBDataPres;
output FlashLED;
output ErrorUSB_read;
reg [19:0] MyCounter;
reg FlashLED;
reg ErrorUSB_read;

always @(posedge QuarterClock)
begin
if (Enable == 1'b1)
	begin
	if (MyCounter == 20'b11111111111111111111)
		begin
		FlashLED = ~FlashLED;
		MyCounter = 20'b00000000000000000000;
		end
	else
		begin
		MyCounter = MyCounter + 1'b1;
		end
	end
else
	begin
	FlashLED = 1'b0;
	end
end	
//When an error occurs, flushing the USB MAC FIFO will help prevent the pc application from hanging. 
always @ (posedge Clock)
begin
	if (Enable == 1'b1)
		begin
		if (USBDataPres == 0)
			begin
			ErrorUSB_read = 1'b1;
			end
		else
			begin
			ErrorUSB_read = 1'b0;
			end
		end
	else
		begin
		ErrorUSB_read = 1'b0;
		end
end
endmodule