/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */

module SwitchCounter (
Clock, 
Reset, 
SwitchTwo, 
SwitchThree,
SwitchFour,
SwitchTwoCount,
SwitchThreeCount,
SwitchFourCount
);

input Clock;
input Reset;
input SwitchTwo;
input SwitchThree;
input SwitchFour;
output [7:0] SwitchTwoCount;
output [7:0] SwitchThreeCount;
output [7:0] SwitchFourCount;
reg [7:0] SwitchTwoCount;
reg [7:0] SwitchThreeCount;
reg [7:0] SwitchFourCount;
reg [19:0] Counter;
reg [1:0] NextState;
reg [1:0] CurState; 
reg SwitchInput;

always @(posedge Clock)
begin
	if (Reset == 0)  //if reset is 0 then we stay in S0.
		begin
		SwitchTwoCount = 8'b00000000;
		SwitchThreeCount = 8'b00000000;
		SwitchFourCount = 8'b00000000;
		end
	else //now things get exciting. . . 
		begin
    	CurState = NextState; // set the next_state to be the present_state by default
		case (CurState)
			2'b00:  //State 0 When the switch is depressed, increment the appropriate counter and then de-bounce it
			if (SwitchTwo == 1'b0)
				begin
				NextState = 2'b01;
				SwitchTwoCount [7:0]  = SwitchTwoCount [7:0] + 1'b1;
				end		
			else if (SwitchThree == 1'b0)
				begin
				NextState = 2'b01;
				SwitchThreeCount [7:0]  = SwitchThreeCount [7:0] + 1'b1;
				end		
			else if (SwitchFour == 1'b0)
				begin
				NextState = 2'b01;
				SwitchFourCount [7:0]  = SwitchFourCount [7:0] + 1'b1;
				end				
			else
				begin 
				NextState = 2'b00;
				end
			2'b01:  //State 1 De-bouncing State 
			if ((Counter == 20'hFFFFF) &&  (SwitchTwo == 1'b1) && (SwitchThree == 1'b1) && (SwitchFour == 1'b1))
				begin
				NextState = 2'b10;
				Counter = 20'h00000;
				end
			else
				begin
				NextState = 2'b01;
				Counter = Counter + 1'b1;
				end
			2'b10:  //State 2 A second de-bouncing state to ensure the switch has fully settled back to it's non-depressed state. 
			if ((Counter == 20'hFFFFF) &&  (SwitchTwo == 1'b1) && (SwitchThree == 1'b1) && (SwitchFour == 1'b1))
				begin
				NextState = 2'b00;
				Counter = 20'h00000;
				end
			else
				begin
				NextState = 2'b10;
				Counter = Counter + 1'b1;
				end	
		endcase
	end //end of else
end // end of always
endmodule