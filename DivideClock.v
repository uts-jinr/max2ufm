/*Copyright � 2002 Altera Corporation. All rights reserved.  Altera products are
protected under numerous U.S. and foreign patents, maskwork rights, copyrights and
other intellectual property laws.  

This reference design file, and your use thereof, is subject to and governed by
the terms and conditions of the applicable Altera Reference Design License Agreement.
By using this reference design file, you indicate your acceptance of such terms and
conditions between you and Altera Corporation.  In the event that you do not agree with
such terms and conditions, you may not use the reference design file. Please promptly
destroy any copies you have made.

This reference design file being provided on an "as-is" basis and as an accommodation 
and therefore all warranties, representations or guarantees of any kind 
(whether express, implied or statutory) including, without limitation, warranties of 
merchantability, non-infringement, or fitness for a particular purpose, are 
specifically disclaimed.  By making this reference design file available, Altera
expressly does not recommend, suggest or require that this reference design file be
used in combination with any other product not provided by Altera. */


module DivideClock (
FastClock,
HalfClock,
QuarterClock,
EighthClock,
SlowClock

);
input FastClock;
output HalfClock;
output QuarterClock;
output EighthClock;
output SlowClock;

reg [7:0] counter;
reg HalfClock;
reg QuarterClock;
reg EighthClock;
reg SlowClock;


always @ (posedge FastClock) 
begin
HalfClock = ~HalfClock; //Period = 30 ns
end

always @ (posedge HalfClock) 
begin
QuarterClock = ~QuarterClock; //Period = 60 ns
end

always @ (posedge QuarterClock) 
begin
EighthClock = ~EighthClock; //Period = 120 ns
end


//We'll also generate a slow clock with a period of 12,000ns
always @ (posedge QuarterClock) 
if (counter == 7'b1100100) // 100 decimal
	begin
	SlowClock = ~SlowClock; 
	counter = 7'b0000000;
	end
else 
	begin 
	counter = 7'b0000000;
	end
endmodule

